var sql = require('mssql');
var async = require('async');
var colors = require('colors');
var config=require('./config/config');
var tablename = "contact_test";
var query = "SELECT a.Selger,a.Kommentar,k.KundeNavn,kp.ForNavn + ' ' + kp.EtterNavn 'Kontakt person',a.AntallTimer,a.TimePris,convert(decimal(10,3),AntallTimer)a.AntallTimer*a.TimePris [Inntekt],case DATEPART(MM,Dato) when 1 THEN '01-Januar'WHEN 2 THEN '02-Februar'WHEN 3 THEN '03-Mars'WHEN 4 THEN '04-April'WHEN 5 THEN '05-Mai'WHEN 6 THEN '06-Juni'WHEN 7 THEN '07-Juli'WHEN 8 THEN '08-August'WHEN 9 THEn '09-September'When 10 THEN '10-Oktober'WHEN 11 THEN '11-November'ELSE '12-Desember'END mnd from Aktivitet a inner join Kunde k on(a.Kundenummer=k.KundeNummer) inner join KontaktPerson kp on (a.KontaktID=kp.KontaktID)   where AktivitetType='Consultant' and AktivitetSubtype='Project' and DATEPART(YYYY,Dato)=2015"//null;//"SELECT COUNT(*) Antall,CompanyName [Kunde],Category1 [Kategori 1],Category2 [Kategori 2],DATEPART(m,CreatedDateTime)[Måned],DATEPART(yy,CreatedDateTime) [År]  FROM vRptIncident WHERE 1=1 GROUP BY CompanyName,Category1,Category2,DATEPART(m,CreatedDateTime),DATEPART(yy,CreatedDateTime)";

async.parallel({
        srcconnect: function(callback) {
            var sourceconnection = new sql.Connection(config.sourceconfig, function(err) {
                if (err) {
                    console.log("Error connecting to source:" + err.red);
                    callback(err);
                }
                var request = new sql.Request(sourceconnection); // or: var request = connection.request(); 
                request.query((query ? query : 'select *  from ' + tablename + ' where 1=0'), function(err, recordset) {
                    if (err) {
                        callback(err);
                    }
                    console.log("Retreived source definition.......".green);
                    var createstmt = createStatement(tablename, recordset.columns);
                    callback(err, {
                        connection: sourceconnection,
                        createstmt: createstmt
                    });
                });
            });

        },
        dstconnect: function(callback) {
            var destinationconnection = new sql.Connection(config.destinationconfig, function(err) {
                if (err) {
                    console.log(("Error connecting to destination:" + err).red);
                    callback(err);
                } else
                    callback(null, {
                        connection: destinationconnection
                    });
            });
        }
    },
    function(err, results) {
        if (err) {
            console.log(err);
            process.exit(1);
        } else {
            var sourceRequest = new sql.Request(results.srcconnect.connection);
            sourceRequest.query(query?query:"SELECT * FROM " + tablename, function(err, recordset) {

                var table = sstable(tablename, recordset.columns);
                table.rows = recordset;
                table.create = true;
                var bulkRequest = new sql.Request(results.dstconnect.connection);
                bulkRequest.bulk(table, function(err, rowcount) {
                    if (err) {
                        console.log(err);
                        process.exit(1);
                    } else {
                        console.log(rowcount + " rows bulkloaded");
                        process.exit(0);
                    }
                });
            })
        }
    });

function sstable(tablename, rmetadata) {
    var table = new sql.Table(tablename);
    for (var name in rmetadata) {
        var tt = rmetadata[name].type;
        console.dir(rmetadata[name]);
        if (tt === sql.NText || tt === sql.Text) {
            table.columns.add(name, sql.VarChar(sql.MAX), {
                nullable: rmetadata[name].nullable
            });
        } else if (rmetadata[name].length)
            table.columns.add(name, rmetadata[name].type(rmetadata[name].length), {
                nullable: rmetadata[name].nullable
            });
        else if (rmetadata[name].precision)
            table.columns.add(name, rmetadata[name].type(rmetadata[name].precision, rmetadata[name].scale), {
                nullable: rmetadata[name].nullable
            });
        else
            table.columns.add(name, rmetadata[name].type, {
                nullable: rmetadata[name].nullable
            });
    }
    return table;
}

function createStatement(tablename, rmetadata) {
    var mm = ["CREATE TABLE", tablename, "("];
    var cols = [];
    for (var name in rmetadata) {
        cols.push(typefromdef(rmetadata[name]));
    }
    mm.push(cols.join(","));
    mm.push(")");
    console.log(mm.join(' '));
}

function typefromdef(columninfo) {
    var retval = {
        name: columninfo.name,
        nullable: columninfo.nullable,
        primarykey: (columninfo.index == 0)
    };
    if (columninfo.type === sql.VarChar) {
        retval.typestr = "varchar";
        retval.length = columninfo.length;
    } else if (columninfo.type === sql.NVarChar) {
        retval.typestr = "nvarchar";
        retval.length = columninfo.length;
    } else if (columninfo.type === sql.NChar) {
        retval.typestr = "nchar";
        retval.length = columninfo.length;
    } else if (columninfo.type === sql.Char) {
        retval.typestr = "char";
        retval.length = columninfo.length;
    } else if (columninfo.type === sql.Int) {
        retval.typestr = "int";

    } else if (columninfo.type === sql.Bit) {
        retval.typestr = "Bit";

    } else if (columninfo.type === sql.Float) {
        retval.typestr = "float";
        retval.precision = columninfo.precision;
        retval.scale = columninfo.scale;
    } else if (columninfo.type === sql.Decimal) {
        retval.typestr = "Decimal";
        retval.precision = columninfo.precision;
        retval.scale = columninfo.scale;
    } else if (columninfo.type === sql.Text || columninfo.type === sql.NText) {
        retval.typestr = columninfo.type === sql.Text ? "varchar" : "nvarchar";
        retval.length = sql.MAX;
    }


    var re = [retval.name, retval.typestr]
    if (retval.length) {
        re.push("(" + (retval.length == "65535" ? "MAX" : retval.length) + ")");
    }
    if (retval.precision) {
        re.push("(" + retval.precision + "," + retval.scale + ")");
    }
    if (retval.primarykey)
        re.push("PRIMARY KEY")
    re.push(re.nullable ? "NULL" : "NOT NULL");
    return re.join(' ');
}
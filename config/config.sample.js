var config = {
    destinationconfig : {
        user: 'user',
        password: 'password',
        server: 'destinationsql',
        database: 'db1',
        options: {
            instanceName: 'DEVELOPMENT'
        }

    }, sourceconfig : {
        user: 'corp.manager',
        domain: "CORP",
        password: "password",
        server: 'sourceserver',
        database: 'db2',
        options: {
            trustedConnection: true
        }
    },
    tablename:"table_test",
    query:"SELECT * FROM tt"
}
module.exports=config;
#sql-copy

Simple tool for copying between two sql server instances. 

##How to use

Copy/rename config.sample.js to config.js. Edit the settings to fit your environment. 

 - destinationconfig - The server to cpoy the data to.
 - sourceconfig - The server to copy from
 - table - Name of table to put the results in
 - query - SQL query for getting the data.

 